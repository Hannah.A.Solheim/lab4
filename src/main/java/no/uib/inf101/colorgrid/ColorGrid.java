package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.List;
import java.util.ArrayList;

public class ColorGrid implements IColorGrid {
    private Color[][] grid;
    private int rows;
    private int cols;

    public ColorGrid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new Color[rows][cols];
    }

@Override
public int rows() {
    return this.rows;
}

@Override
public int cols() {
    return this.cols;
}

@Override
    public List<CellColor> getCells() {
        List<CellColor> cellColors = new ArrayList<>();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                CellPosition pos = new CellPosition(row, col);
                Color color = this.grid[row][col];
                cellColors.add(new CellColor(pos, color));
            }
        }
        return cellColors;
    }

    @Override
    public Color get(CellPosition pos) {
        return this.grid[pos.row()][pos.col()];
    }

    @Override
    public void set(CellPosition pos, Color color) {
        this.grid[pos.row()][pos.col()] = color;
    }
}
