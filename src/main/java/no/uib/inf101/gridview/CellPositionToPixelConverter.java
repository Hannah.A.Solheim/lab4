package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }
  public Rectangle2D getBoundsForCell(CellPosition cp){
    double boxX = box.getX();
    double boxY = box.getY();
    double boxWidth = box.getWidth();
    double boxHeight = box.getHeight();
    double col = cp.col();
    double row = cp.row();
    double cols = gd.cols();
    double rows = gd.rows();

    double cellWidth = (boxWidth - (margin * (cols + 1))) / cols;
    double cellHeight = (boxHeight - (margin * (rows + 1))) / rows;
    double cellX = boxX + (margin * (col + 1)) + (cellWidth * (col));
    double cellY = boxY + (margin * (row + 1)) + (cellHeight * (row));

    Rectangle2D cell = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    return cell;
  }
}

