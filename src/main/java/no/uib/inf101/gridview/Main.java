package no.uib.inf101.gridview;

import java.awt.Color;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
  public static void main(String[] args) {
    ColorGrid grid = new ColorGrid(3, 4);
    GridView gridView = new GridView(grid);
    JFrame frame = new JFrame();
    frame.setContentPane(gridView);
    frame.setTitle("INF101");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

    CellPosition pos1 = new CellPosition(0, 0);
    CellPosition pos2 = new CellPosition(0, 3);
    CellPosition pos3 = new CellPosition(2, 0);
    CellPosition pos4 = new CellPosition(2, 3);

    grid.set(pos1, Color.RED);
    grid.set(pos2, Color.BLUE); 
    grid.set(pos3, Color.YELLOW);
    grid.set(pos4, Color.GREEN);
    
  }
}
