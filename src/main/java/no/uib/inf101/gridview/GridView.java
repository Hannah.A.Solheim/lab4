package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel{

    int width;
    int height;
    IColorGrid colorGrid;

  public GridView(IColorGrid colorGrid){
    this.width = 400;
    this.height = 300;
    this.colorGrid = colorGrid;
    this.setPreferredSize(new Dimension(width, height));
  }
    
  @Override  
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  } 

    private static final double OUTERMARGIN = 30;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  private void drawGrid(Graphics2D g2) {
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double x2 = width - 2 * OUTERMARGIN;
    double y2 = height - 2 * OUTERMARGIN;
    g2.setColor(MARGINCOLOR);
    g2.fill(new Rectangle2D.Double(x, y, width, height));

    CellPositionToPixelConverter pixel = new CellPositionToPixelConverter(null, colorGrid, OUTERMARGIN);
    drawCells(g2, colorGrid, pixel);
    }

    private static void drawCells(Graphics2D g2, CellColorCollection colors, CellPositionToPixelConverter pixelConverter) {
        for (CellColor cell : colors.getCells()){
            CellPosition pos = cell.cellPosition();
            Color color = cell.color() != null ? cell.color() : Color.DARK_GRAY;
            Rectangle2D cellRect = pixelConverter.getBoundsForCell(pos);

            g2.setColor(color);
            g2.fill(new Rectangle2D.Double(cellRect.getX(), cellRect.getY(), cellRect.getWidth(), cellRect.getHeight()));
        }
    }
}
